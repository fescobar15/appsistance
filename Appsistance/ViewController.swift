//
//  ViewController.swift
//  Appsistance
//
//  Created by FELIPE ESCOBAR AVILA on 8/16/19.
//  Copyright © 2019 FELIPE ESCOBAR AVILA. All rights reserved.
//

import UIKit
import CoreBluetooth


class ViewController: UIViewController {
    
    var peripheralManager: CBPeripheralManager!

    @IBOutlet weak var button1: UIButton!
    
    @IBOutlet weak var button2: UIButton!
    
    @IBOutlet weak var button3: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button1.layer.cornerRadius=10
        
        button1.layer.borderWidth=2
        
        button1.layer.borderColor = UIColor.red.cgColor
        
        button2.layer.cornerRadius=10
        
        button2.layer.borderWidth=2
        
        button2.layer.borderColor = UIColor.red.cgColor
        
        button3.layer.cornerRadius=10
        
        button3.layer.borderWidth=2
        
        button3.layer.borderColor = UIColor.red.cgColor
        
        self.view.addSubview(button1)
        self.view.addSubview(button2)
        self.view.addSubview(button3)

    }

    @IBAction func presente(_ sender: Any) {
        
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        print("Started advertising")

    }
    
    
    @IBAction func apagar(_ sender: Any) {
        peripheralManager?.stopAdvertising();
    }
    
    
    @IBAction func verificar(_ sender: Any) {
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "d-M-yyyy"
        let fecha = format.string(from: date)
        let arreglo = fecha.components(separatedBy: "-")
        let dia = arreglo[0]
        let mes = String(Int(arreglo[1])! - 1)
        let anio = arreglo[2]
        
        let alertController1=UIAlertController (title: "Bien", message: "Ya quedo registrado", preferredStyle: .alert)
        
        alertController1.addAction(UIAlertAction (title: "Okas lokas", style: .default))
        
        let alertController2=UIAlertController (title: "Paila", message: "No esta registrado", preferredStyle: .alert)
        
        alertController2.addAction(UIAlertAction (title: "Okas lokas", style: .default))
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + dia + "-" + mes + "-" + anio + "/students/201531251")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else { self.present(alertController2, animated: false, completion: nil); return}
            
            self.present(alertController1, animated: false, completion: nil)
        }
        task.resume()

    }
    
}

extension ViewController: CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print(peripheral.state.rawValue)
        
        if(peripheral.state.rawValue == CBManagerState.poweredOn.rawValue)
        {
            let advertisementData = [CBAdvertisementDataLocalNameKey: "Felipe Escobar"]
            
            peripheralManager?.startAdvertising(advertisementData)
        }
        
    }
    
}

